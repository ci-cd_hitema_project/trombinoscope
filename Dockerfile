# Utilisez une image Node.js LTS comme base
FROM node:lts-alpine AS build

# Définissez le répertoire de travail
WORKDIR /app

# Copiez les fichiers de package.json et package-lock.json pour installer les dépendances
COPY trombinoscope/package*.json ./

# Installez les dépendances
RUN npm install

# Copiez tous les fichiers du dossier trombinoscope dans le conteneur
COPY trombinoscope/ .

# Construisez l'application React
RUN npm run build

# Utilisez une image légère Nginx comme image finale
FROM nginx:alpine

# Copiez les fichiers construits depuis le conteneur de construction vers le répertoire Nginx public
COPY --from=build /app/build /usr/share/nginx/html

# Exposez le port 80
EXPOSE 80

# Commande pour démarrer Nginx une fois que le conteneur est lancé
CMD ["nginx", "-g", "daemon off;"]
