import React from 'react';
import Trombinoscope from './trombinoscope';
import './App.css';

function App() {
  return (
    <div className="App">
      <Trombinoscope />
    </div>
  );
}

export default App;
