import React, { useState } from 'react';
import PersonDetails from './PersonDetails';
import './person.css';

const Person = ({ name, image, onEdit, onDelete, onDetails }) => {
  const [showDetails, setShowDetails] = useState(false);

  const handleDetailsClick = () => {
    setShowDetails(true);
  };

  const handleCloseDetails = () => {
    setShowDetails(false);
  };

  return (
    <div className="person">
      <img src={`./images/${image}`} alt={"(" + name + " avatar)"} className="person-image" />
      <p onClick={() => (onDetails ? onDetails(name) : handleDetailsClick())} className="person-name">
        {name}
      </p>
      <div className="person-buttons">
        <button onClick={onEdit} className="edit-button">
          Modifier
        </button>
        <button onClick={onDelete} className="delete-button">
          Supprimer
        </button>
      </div>

      {showDetails && (
        <PersonDetails name={name} onClose={handleCloseDetails} />
      )}
    </div>
  );
};

export default Person;
