import React from 'react';
import './PersonDetails.css';

const PersonDetails = ({ person, onClose }) => {
  return (
    <div className="person-details">
      <div className="details-content">
        <p>Nom: {person.name}</p>
        {/* Ajoutez d'autres informations si nécessaire */}
      </div>
      <button onClick={onClose} className="close-button">
        Fermer
      </button>
    </div>
  );
};

export default PersonDetails;
