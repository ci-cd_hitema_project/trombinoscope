import React, { useState, useEffect } from "react";
import "./EditPersonForm.css";

const EditPersonForm = ({ onClose, selectedPerson, updatePerson }) => {
  const [editedPerson, setEditedPerson] = useState({
    name: selectedPerson ? selectedPerson.name : "",
    image: selectedPerson ? selectedPerson.image : "",
  });

  useEffect(() => {
    // Met à jour les champs du formulaire lorsque selectedPerson change
    setEditedPerson({
      name: selectedPerson ? selectedPerson.name : "",
      image: selectedPerson ? selectedPerson.image : "",
    });
  }, [selectedPerson]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEditedPerson((prevPerson) => ({ ...prevPerson, [name]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Utilise la fonction updatePerson pour mettre à jour la personne
    updatePerson(editedPerson);

    onClose();
  };

  const imageOptions = ["man.png", "woman.png"];

  return (
    <div className="edit-person-form">
      <form onSubmit={handleSubmit}>
        <label>Nom:</label>
        <input
          type="text"
          name="name"
          value={editedPerson.name}
          onChange={handleChange}
          required
        />

        <label>Image Name:</label>
        <select
          name="image"
          value={editedPerson.image || ""}
          onChange={handleChange}
          required
        >
          <option value="" disabled>
            Sélectionnez une image
          </option>
          {imageOptions.map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </select>

        <button type="submit">Modifier</button>
        <button type="button" onClick={onClose}>
          Annuler
        </button>
      </form>
    </div>
  );
};

export default EditPersonForm;
