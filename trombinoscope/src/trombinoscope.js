import React, { useState, useEffect } from "react";
import Person from "./components/person";
import AddPersonForm from "./components/AddPersonForm";
import EditPersonForm from "./components/EditPersonForm";
import PersonDetails from "./components/PersonDetails";
import "./trombinoscope.css";

const Trombinoscope = () => {
  const [people, setPeople] = useState([]);
  const [showAddForm, setShowAddForm] = useState(false);
  const [showEditForm, setShowEditForm] = useState(false);
  const [detailsPerson, setDetailsPerson] = useState(null);
  const [selectedPerson, setSelectedPerson] = useState(null);

  useEffect(() => {
    const storedPeople = JSON.parse(localStorage.getItem("people"));

    if (storedPeople && storedPeople.length > 0) {
      setPeople(storedPeople);
    } else {
      const peopleSample = [
        { name: "John Doe", image: "man.png" },
        { name: "Kate Doe", image: "woman.png" },
        { name: "Mike Price", image: "man.png" },
        { name: "Donna Price", image: "woman.png" },
        { name: "Fred Riley", image: "man.png" },
        { name: "Patricia Riley", image: "woman.png" },
        { name: "Jack Johnson", image: "man.png" },
        { name: "Hilary Johnson", image: "woman.png" },
      ];

      setPeople(peopleSample);
      localStorage.setItem("people", JSON.stringify(peopleSample));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("people", JSON.stringify(people));
  }, [people]);

  const addPerson = (person) => {
    setPeople((prevPeople) => [...prevPeople, person]);
  };

  const updatePerson = (person) => {
    const updatedPeople = [...people];
    const existingPersonIndex = people.findIndex(
      (p) => p.name === selectedPerson.name
    );

    if (existingPersonIndex !== -1) {
      if (selectedPerson.name !== person.name) {
        updatedPeople[existingPersonIndex].name = person.name;
      }
      updatedPeople[existingPersonIndex].image = person.image;

      setPeople(updatedPeople);
    }
  };

  const handleAddClick = () => {
    setShowAddForm(true);
    setShowEditForm(false);
    setSelectedPerson(null);
    setDetailsPerson(null);
  };

  const handleEditClick = (person) => {
    setShowAddForm(false);
    setShowEditForm(true);
    setSelectedPerson(person);
    setDetailsPerson(null);
  };

  const handleDeleteClick = (person) => {
    setPeople((prevPeople) => prevPeople.filter((p) => p.name !== person.name));
  };

  const handleDetailsClick = (person) => {
    setShowAddForm(false);
    setShowEditForm(false);
    setSelectedPerson(null);
    setDetailsPerson(person);
  };

  const handleFormClose = () => {
    setShowAddForm(false);
    setShowEditForm(false);
    setSelectedPerson(null);
    setDetailsPerson(null);
  };

  return (
    <div className="trombinoscope-container">
      <div className="crud-buttons">
        <button onClick={handleAddClick}>Ajouter</button>
      </div>

      {showAddForm && (
        <AddPersonForm onClose={handleFormClose} addPerson={addPerson} />
      )}

      {showEditForm && (
        <EditPersonForm
          onClose={handleFormClose}
          selectedPerson={selectedPerson}
          updatePerson={updatePerson}
        />
      )}

      {detailsPerson && (
        <PersonDetails
          person={detailsPerson}
          onClose={() => setDetailsPerson(null)}
        />
      )}

      <div className="trombinoscope">
        {people.map((person, index) => (
          <Person
            key={index}
            {...person}
            onEdit={() => handleEditClick(person)}
            onDelete={() => handleDeleteClick(person)}
            onDetails={() => handleDetailsClick(person)}
          />
        ))}
      </div>
    </div>
  );
};

export default Trombinoscope;
